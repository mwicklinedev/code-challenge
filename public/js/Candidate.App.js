const app = angular.module("Candidate.App", []);

randomColor = () =>{
    getRandomInt = (max) =>{
        return Math.floor(Math.random() * Math.floor(max));
    }
    const colors = [
        "#049CB8",
        "#00B7B0",
        "#FF7C00",
        "#FF7100",
        "#E66600",
        "#008B86"
    ]
    return colors[getRandomInt(6)];
}

app.component("itmRoot", {
    controller: class {
        constructor() {
            this.candidates = [
                { name: "Puppies", votes: 10, displayColor: { color: randomColor()}},
                { name: "Kittens", votes: 12, displayColor: { color: randomColor()}},
                { name: "Gerbils", votes: 7, displayColor: { color: randomColor()}}
            ];

            this.totalVotes = 0;
            this.candidates.forEach(item => this.totalVotes += item.votes);

            this.highestVotedCandidate = {};
            this.checkMostVotes();
        }

        checkMostVotes(){
            let tempCount = 0;
            let highestIndex = 0;
            this.candidates.forEach((item, index)=>{
                if(item.votes > tempCount){
                    highestIndex = index;
                    tempCount = item.votes;
                }
            });
            this.highestVotedCandidate = this.candidates[highestIndex];
        }

        onVote(candidate) {
            candidate.votes ++;
            if(candidate.votes > this.highestVotedCandidate.votes){
                this.checkMostVotes();
            }
            this.totalVotes ++;
            console.log(`Vote for ${candidate.name}`);
        }

        onAddCandidate(candidate) {
            this.checkMostVotes();
            console.log(`Added candidate ${candidate.name}`);
        }

        onRemoveCandidate(candidate) {
            this.totalVotes -= candidate.votes;
            console.log(`Removed candidate ${candidate.name}`);
        }
    },
    template: `
        <h1>Which candidate brings the most joy?</h1>
        <hr>
        <span ng-if="$ctrl.candidates.length > 0">
            <h2 id="lead-header">&#126; {{$ctrl.highestVotedCandidate.name}} is in the lead with {{$ctrl.highestVotedCandidate.votes}} &#126;</h2>
        </span>
        <div class="container">
            <div class="row">
                <div class="col-lg">
                    <itm-results 
                        candidates="$ctrl.candidates"
                        total-votes="$ctrl.totalVotes">
                    </itm-results>
                </div>
                <div id="vote-column" class="col-lg">
                    <itm-vote 
                        candidates="$ctrl.candidates"
                        on-vote="$ctrl.onVote($candidate)">
                    </itm-vote>
                </div>
                <div id="manage-column" class="col-lg">
                    <itm-management 
                        candidates="$ctrl.candidates"
                        on-add="$ctrl.onAddCandidate($candidate)"
                        on-remove="$ctrl.onRemoveCandidate($candidate)">
                    </itm-management>
                </div>
            </div>
        </div>
    `
});

app.component("itmManagement", {
    bindings: {
        candidates: "<",
        onAdd: "&",
        onRemove: "&"
    },
    controller: class {
        constructor() {
            this.newCandidateName = "";
            this.msg = "";
        }

        submitCandidate(candidateName) {
            let findSameName = () => { //helper function to see if name is already taken
                for(let x in this.candidates){
                    if(this.candidates[x].name.toLowerCase() === candidateName.toLowerCase()) return true;
                }
                return false;
            }

            if(!candidateName){
                this.msg = "You must enter a name";
            } else if(findSameName()){ //call to helper function
                this.msg="Name is already taken";
            } else{
                if(candidateName.length > 18) candidateName = candidateName.slice(0, 18); //trim length of string
                const candidate = {name: candidateName, votes: 0, displayColor: {color: randomColor()}}
                this.candidates.push(candidate);
                this.onAdd({ $candidate: candidate});
                this.newCandidateName = "";
                this.msg= "";
            }
        }

        removeCandidate(candidate) {
            const index = this.candidates.indexOf(candidate);
            this.candidates.splice(index, 1);
            this.onRemove({ $candidate: candidate });
        }
    },
    template: `
        <h2>Manage Candidates</h2>

        <h3>Add New Candidate</h3>
        <form ng-submit="$ctrl.submitCandidate($ctrl.newCandidateName)" novalidate>
            <div class="form-group">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-10 small-padding">
                            <input type="text" class="form-control" ng-model="$ctrl.newCandidateName" placeholder="Candidate Name" required>
                        </div>
                        <div class="col-2 small-padding">
                            <button id="add-btn" class="btn btn-primary" type="submit">Add</button>
                        </div>
                    </div>
                </div>
                <p ng-bind="$ctrl.msg"></p>
            </div>
        </form>

        <h3>Remove Candidate</h3>
            <ul id="remove-list">
                <li ng-repeat="candidate in $ctrl.candidates">
                    <span ng-style="candidate.displayColor" ng-bind="candidate.name"></span>&#160;
                    <a class="remove-x"
                    ng-click="$ctrl.removeCandidate(candidate)">X</a>
                </li>
            </ul>

    `
});

app.component("itmVote", {
    bindings: {
        candidates: "<",
        onVote: "&"
    },
    controller: class {},
    template: `
        <h2 id="vote-header">Cast your vote!</h2>

        <button type="button"
            class="btn"
            ng-repeat="candidate in $ctrl.candidates"
            ng-style="{backgroundColor: candidate.displayColor.color}"
            ng-click="$ctrl.onVote({ $candidate: candidate })">
            <span ng-bind="candidate.name"></span>
        </button>
    `
});

app.component("itmResults", {
    bindings: {
        candidates: "<",
        totalVotes: "<"
    },
    controller: class {
        constructor(){}
        roundNumber(input){
            return Math.round(input) || 0;
        }
    },
    template: `
        <h2>Live Results</h2>
        <ul>
            <li ng-repeat="candidate in $ctrl.candidates | orderBy : 'votes':true" >
                <div ng-style="candidate.displayColor">
                    <span ng-bind="candidate.name"></span>
                    <strong ng-bind="candidate.votes"></strong>
                    <b>&hyphen;  {{$ctrl.roundNumber(candidate.votes / $ctrl.totalVotes * 100)}}&percnt; of total</b>
                </div>
            </li>
        </ul>
    `
});
